<?php
include_once __DIR__ . "/../Config/Koneksi.php";
include_once __DIR__ . '/Motor.php';
class Mahasiswa
{
    public $nim;
    public $nama;
    public $tanggalLahir;
    public $alamat;
    public $jenisKelamin;
    public $motors;

    /**
     * Ini adalah fungsi untuk mengambil semua data dari table mahasiswa
     * @return array data mahasiswa
     */
    public static function getAll(): array
    {
        $query = "select * from mahasiswa";
        $conn = new Koneksi();
        $mq =  mysqli_query($conn->koneksi, $query);
        $result = [];
        while ($mhsDB = mysqli_fetch_object($mq)) {
            $mhs = new Mahasiswa();
            $mhs->nim = $mhsDB->nim;
            $mhs->nama = $mhsDB->nama;
            $mhs->tanggalLahir = $mhsDB->tgl_lahir;
            $mhs->alamat = $mhsDB->alamat;
            $mhs->jenisKelamin = $mhsDB->jenis_kelamin;
            $mhs->motors = Motor::getByNimMahasiswaTanpaDataMahasiswa($mhsDB->nim);
            $result[] = $mhs;
        }
        return $result;
    }

    public static function getByPrimaryKey($nim): ?object
    {
        $query = "select * from mahasiswa where nim='$nim'";
        $conn = new Koneksi();
        $mq =  mysqli_query($conn->koneksi, $query);
        $result = null;
        while ($mhsDB = mysqli_fetch_object($mq)) {
            $mhs = new Mahasiswa();
            $mhs->nim = $mhsDB->nim;
            $mhs->nama = $mhsDB->nama;
            $mhs->tanggalLahir = $mhsDB->tgl_lahir;
            $mhs->alamat = $mhsDB->alamat;
            $mhs->jenisKelamin = $mhsDB->jenis_kelamin;
            $mhs->motors = Motor::getByNimMahasiswa($mhsDB->nim);
            $result = $mhs;
        }
        return $result;
    }

    public function insert()
    {
        $query = "insert into mahasiswa values ("
            . "'$this->nim',"
            . "'$this->nama',"
            . "'$this->tanggalLahir',"
            . "'$this->alamat',"
            . "'$this->jenisKelamin'"
            . ")";
        $conn = new Koneksi();
        mysqli_query($conn->koneksi, $query);
    }

    public function delete()
    {
        $query = "delete from mahasiswa where nim='$this->nim'";
        $conn = new Koneksi();
        mysqli_query($conn->koneksi, $query);
    }

    public function update()
    {
        $query = "update mahasiswa set "
            . "nama ='$this->nama',"
            . "tgl_lahir = '$this->tanggalLahir',"
            . "alamat = '$this->alamat',"
            . "jenis_kelamin = '$this->jenisKelamin' "
            . "where nim='$this->nim'";
        // var_dump($query);
        // exit;
        $conn = new Koneksi();
        mysqli_query($conn->koneksi, $query);
    }
}
