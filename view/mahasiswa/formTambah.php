<!DOCTYPE html>
<html lang="en">

<head>
    <title>Form Tambah Mahasiswa</title>
</head>

<body>
    <h2>Tambah Mahasiswa</h2>
    <form method="POST" action="prosesTambah.php">
        <p>Nim <br> <input required type="text" name="nim" /></p>
        <p>Nama <br> <input required type="text" name="nama" /></p>
        <p>Tanggal Lahir <br> <input required type="date" name="tanggalLahir" /></p>
        <p>Alamat <br> <input required type="text" name="alamat" /></p>
        <p>Jenis Kelamin <br>
            <input required type="radio" name="jenisKelamin" value="L" /> Laki Laki
            <input required type="radio" name="jenisKelamin" value="P" /> Perempuan
        </p>
        <button type="submit">Simpan</button>
    </form>
</body>

</html>