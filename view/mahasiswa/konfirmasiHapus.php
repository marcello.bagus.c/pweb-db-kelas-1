<?php
include_once __DIR__ . '/../../Model/Mahasiswa.php';
$nim = $_REQUEST['nim'];
$mhs = Mahasiswa::getByPrimaryKey($nim);
if ($mhs === null) {
    echo "<h2>Data Mahasiswa tidak ditemukan</h2>";
    echo "<a href='index.php'>Klik Link Ini untuk kembali</a>";
    die();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Document</title>
</head>

<body>
    <h2>Anda yakin hapus data ini?</h2>
    <p>Nim : <?= $mhs->nim ?></p>
    <p>Nama : <?= $mhs->nama ?></p>
    <p>Tanggal lahir : <?= $mhs->tanggalLahir ?></p>
    <p>Jenis Kelamin : <?= $mhs->jenisKelamin ?></p>
    <p>Alamat : <?= $mhs->alamat ?></p>
    <a href="index.php">Batal</a>
    <a href="prosesHapus.php?nim=<?= $mhs->nim ?>">Hapus</a>
</body>

</html>