<?php
include_once __DIR__ . '/../../Model/Mahasiswa.php';
$nim = $_REQUEST['nim'];
$mhs = Mahasiswa::getByPrimaryKey($nim);
if ($mhs === null) {
    echo "<h2>Data Mahasiswa tidak ditemukan</h2>";
    echo "<a href='index.php'>Klik Link Ini untuk kembali</a>";
    die();
} else {
    #function untuk proses hapus
    $mhs->delete();
    #redirect ke halaman index
    header('Location: index.php');
}
