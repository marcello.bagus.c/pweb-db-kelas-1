<?php
include_once __DIR__ . '/../../Model/Mahasiswa.php';
$nim = $_REQUEST['nim'];
$mhs = Mahasiswa::getByPrimaryKey($nim);
if ($mhs === null) {
    echo "<h2>Data Mahasiswa tidak ditemukan</h2>";
    echo "<a href='index.php'>Klik Link Ini untuk kembali</a>";
    die();
} else {
    #function untuk proses hapus
    $nama = $_REQUEST['nama'];
    $alamat = $_REQUEST['alamat'];
    $tanggalLahir = $_REQUEST['tanggalLahir'];
    $jenisKelamin = $_REQUEST['jenisKelamin'];

    #2. set semua fieldnya
    $mhs->nama = $nama;
    $mhs->alamat = $alamat;
    $mhs->tanggalLahir = $tanggalLahir;
    $mhs->jenisKelamin = $jenisKelamin;

    #3. Panggil fungsi update
    $mhs->update();

    #redirect ke halaman index
    header('Location: index.php');
}
