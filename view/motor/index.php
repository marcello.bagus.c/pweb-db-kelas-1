<?php
include_once __DIR__ . '/../../Model/Motor.php';
$listMotor = Motor::getAll();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Motor</title>
</head>

<body>
    <h3>Data Motor Mahasiswa</h3>
    <table border="1" width='100%'>
        <thead>
            <tr>
                <th>No</th>
                <th>Plat Nomer</th>
                <th>Merek</th>
                <th>Tipe</th>
                <th>Pemilik</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            foreach ($listMotor as $motor) {
            ?>
                <tr>
                    <td><?= $no++ ?></td>
                    <td><?= $motor->platNo ?></td>
                    <td><?= $motor->merek ?></td>
                    <td><?= $motor->tipe ?></td>
                    <td>
                        <?= $motor->mahasiswa->nama ?> /
                        <?= $motor->mahasiswa->nim ?>
                    </td>
                    <td>
                        <a href="formUbah.php?id=<?=$motor->id ?>">Edit</a>
                        | Delete
                    </td>
                </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</body>

</html>