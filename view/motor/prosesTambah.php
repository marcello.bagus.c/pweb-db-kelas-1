<?php
include_once __DIR__ . '/../../Model/Motor.php';

#1.Ambil semua parameter form
$platNo = $_REQUEST['platNo'];
$merek = $_REQUEST['merek'];
$tipe = $_REQUEST['tipe'];
$mahasiswaNim = $_REQUEST['mahasiswaNim'];

#2. Buat Objek dari Motor
$motor = new Motor();
$motor->platNo = $platNo;
$motor->merek = $merek;
$motor->tipe = $tipe;
$motor->mahasiswaNim = $mahasiswaNim;

#3. Panggil function insert via objek
$motor->insert();

#4. Redirect ke halaman list motor
header('Location: index.php');
